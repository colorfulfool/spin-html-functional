# File IO made functional

def readlines(filename):
  return (line for line in open(filename, "r"))
  
def read(filename):
  return open(filename, "r").read()
  
def write(filename, content):
  with open(filename, "w") as file:
    file.write(content)
  
# Function application  
  
def rpartial(function, *rest_of_arguments):
  return lambda first_argument: function(*([first_argument] + list(rest_of_arguments)))
  
# Data composition

def merge(dicts):
  """
  >>> merge([{'AAA': 'first'}, {'BBB': 'second', 'CCC': 'second'}])
  {'AAA': 'first', 'BBB': 'second', 'CCC': 'second'}
  """
  from functools import reduce
  return reduce(lambda x,y: {**x, **y}, dicts)