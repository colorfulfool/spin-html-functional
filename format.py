class Format:
  def __init__(self, name, template_name, source_directory):
    self.name = name
    self.source_directory = source_directory
    self.template_name = template_name
    
  def template_path(self):
    return self.source_directory + self.template_name
    
  def template(self):
    return open(self.template_path()).read()
    
  def suffix(self):
    return "." + self.name