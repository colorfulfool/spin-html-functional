import Path
import File

defmodule Config do
  def placeholders(config) do
    config |> section("Placeholders") |> map(parse_line)
  end
  
  def subdirectories(root) do
    root |> wildcard("*") |> filter(dir?)
  end
  
  defp section(text, section_name) do
    names = Regex.scan(r{^\[(.+)\]$}, text)
    sections = Regex.split(r{^\[(.+)\]$}, text)
    map = zip(names, sections) |> Map.new(fn [k, v] -> {k, v} end)
    Map.fetch map, section_name
  end
end

import ExUnit.Assertions
import Config

assert Config.parse_line("") == ["XXX", ["Ampthill", "Arlesey", "Bedford"], per_page]

assert Config.templates("") == [
  [".html", "<div class=\"title\">Hello XXX, YYY! How's it going in XXX?</div>"],
  ["AMP.html", "<h1>Hello XXX, YYY! How's it going in XXX?</h1>"]
]