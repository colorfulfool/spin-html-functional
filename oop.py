# pipe(
#   template,
#   lambda page: Unspin().compute(page),
#   lambda page: Placeholders(placeholders).compute(page),
#   lambda page: Pictures(source_directory).compute(page)
#   lambda page: Save(output_dir).compute(page)
# )

config = Config(open(project_directory + "project.cfg").read())

for format in config.formats():
  for page in format.template() * config.number_of_pages():
    compute(
      page,
      Unspin(),
      Evaluate(config.syntaxes()),
      Save(config.output_directory(), format.suffix())
    )
    
class Syntax:
  def evaluate_on(self, page):
    occurence_number = 0
    def sub_occurence(match):
      occurence_number += 1
      return self.value(page.number(), occurence)
    
    return re.sub(self.pattern, sub_occurence, rendered)
    
class FunctionCall(Syntax):
  def __init__(self, pattern, function):
    self.pattern = pattern
    self.function = function
    
  def value(self, *args, **kwargs):
    return self.function(*args, **kwargs)
    
class Lookup(Syntax):
  indexers = {
    "per_page": lambda page, invocation: page,
    "one_off":  lambda page, invocation: page + invocation
  }
  
  def __init__(self, pattern, values, indexer_name):
    self.pattern = pattern
    self.values = values
    self.indexer = indexers[indexer_name]
    
  def value(self, *args, **kwargs):
    return self.indexer(*args, **kwargs)
    
class Evaluate:
  def __init__(self, syntaxes):
    self.syntaxes = syntaxes
  
  def compute(self, page):
    return reduce(lambda page, syntax: syntax.evaluate_on(page), 
      self.syntaxes, page)
    
class Save:
  def __init__(self, output_dir, suffix):
    self.output_dir = output_dir
    self.suffix = suffix
    
  def output_file(self, page):
    return self.output_dir + page.number + self.suffix
    
  def compute(self):
    with open(self.output_file(page), "w") as file:
      file.write(page.content)