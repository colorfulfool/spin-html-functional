# generator(table('Cars.csv'), ['VVV'], 'take')
# => { 'VVV': lambda: table('Cars.csv').take()['VVV'] }

# evaluate_generators('Once XXX, twice RRR and RRR', {'XXX': lambda: 'first', 'RRR': lambda: 'second'})
# => 'Once first, twice second and second'

from re import search, sub

def table(file):
  """
  >>> list( table('test/Town County List.csv') )
  [{'XXX': 'Ampthill', 'YYY': 'Bedfordshire', 'RRR': 'Woburn Rd'}]
  """
  from csv import DictReader
  return (
    dict(row)
    for row in DictReader(open(file, "r"))
  )

def parse_config_line(line):
  """
  >>> parse_config_line('Town County List.csv = next XXX,YYY,RRR') # doctest: +ELLIPSIS
  (<...>, ['XXX', 'YYY', 'RRR'], 'next')
  """
  match = search(r'(?P<file>.+) = (?P<access>\w+) (?P<placeholders>\S+)', line)
  
  file = match.group('file')
  placeholders = match.group('placeholders').split(',')
  access = match.group('access')
  
  return (table(file), placeholders, access)

  def generator(args_tuple):
  """
  >>> takes = generator(([{'VVV': 'one', 'QQQ': 'uno'}, {'VVV': 'two', 'QQQ': 'des'}], ['VVV', 'QQQ'], 'next')); \
      takes['VVV'](); takes['VVV'](); takes['QQQ'](); takes['QQQ']();
  'one'
  'two'
  'uno'
  'des'
  >>> peeks = generator(([{'VVV': 'one', 'QQQ': 'uno'}, {'VVV': 'two', 'QQQ': 'des'}], ['VVV', 'QQQ'], 'peek')); \
      peeks['VVV'](); peeks['VVV'](); peeks['QQQ'](); peeks['QQQ']();
  'one'
  'one'
  'uno'
  'uno'
  """
  
  source, placeholders, access = args_tuple
  
  from itertools import cycle
  from more_itertools import peekable
  iterator = peekable(cycle(source))
  
  def fetch_from_source(placeholder):
    return lambda: getattr(iterator, access)()[placeholder]
  
  return {
    placeholder: fetch_from_source(placeholder) 
    for placeholder in placeholders
  }
  
def evaluate_generators(template, generators):
  """
  >>> evaluate_generators('Once XXX, twice RRR and RRR', 
  ...  {'XXX': lambda: 'first', 'RRR': lambda: 'second'})
  'Once first, twice second and second'
  """
  rendered = template
  for placeholder, function in generators.items():
    rendered = sub(placeholder, lambda match: function(), rendered)
  return rendered