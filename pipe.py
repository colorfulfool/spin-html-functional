def pipe(start, *functions):
  """
  >>> pipe(
  ...   'Anton', 
  ...   lambda name: name + "1", 
  ...   lambda name: name + "2"
  ... )
  'Anton12'
  """
  from functools import reduce
  return reduce(lambda value, function: function(value), functions, start)