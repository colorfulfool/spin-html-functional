Code.load_file("placeholder.ex")

import List
import Enum

access = fn list, indexer ->
  fn page, invocation, syntax -> 
    Enum.at list, indexer.(page, invocation, syntax)
  end
end

per_page = fn page, _, _ -> page end
one_off = fn page, invocation, _ -> page + invocation end
evaluate_spintax = fn page, _, syntax -> Spintax.evaluate(syntax, choose: page) end

config_placeholders = Config.placeholders(config) |> map fn [label, values, indexer] ->
  [label, access.(values, eval_string(indexer))]
end

picture_placeholders = Config.subdirectories(source_directory) |> map fn directory ->
  [~r/[picture from #{basename directory}]/, access.(image_tags_from(directory), one_off)]
end

builtin_placeholders = [
  [~r/{.+?}/, evaluate_spintax]
]

placeholders = config_placeholders ++ picture_placeholders ++ builtin_placeholders

templates = Config.templates(config)

number_of_pages = 3

pages = map templates, fn [_, template] ->
  map Range.new(0, number_of_pages-1), fn page_number ->
    foldl placeholders, template, fn [label, access_value], template ->
      Placeholder.render(page_number, template, label, access_value)
    end
  end
end

IO.inspect(pages)