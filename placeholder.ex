import List
import Enum

defmodule Placeholder do
  def render(page_number, template, pattern, access_value) do
    {_, expression} = Regex.compile(pattern)
    
    invocations_in_template = flatten Regex.scan(expression, template)
    
    insertions = map with_index(invocations_in_template), fn {invocation, invocation_number} -> 
      [invocation, access_value.(page_number, invocation_number, invocation)]
    end
    
    foldl insertions, template, fn [invocation, insertion], template ->
      String.replace(template, invocation, insertion, global: false)
    end
  end
end

import ExUnit.Assertions

access_value = fn page, _, _ -> Enum.at(["Ampthill", "Arlesey", "Bedford"], page) end

assert Placeholder.render(1, "<h1>Hello XXX, YYY! How's it going in XXX?</h1>", "XXX", access_value) == "<h1>Hello Arlesey, YYY! How's it going in Arlesey?</h1>"