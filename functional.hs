template = "<h1>Hello XXX! How's it going in XXX?</h1>"

label = "XXX"

values = ["Ampthill", "Arlesey", "Bedford"]

invocationsInTemplate = [label, label]

invocations :: [[String]]
invocations = replicate (length values) invocationsInTemplate

replace :: [String] -> String -> [String]
replace invocs value = map (const value) invocs

applied = zipWith replace invocations values