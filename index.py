def wrapping_get(list, index):
  """
  >>> wrapping_get(['one', 'two', 'three'], 4)
  'two'
  """
  return list[index % len(list)]

def access(values, indexer):
  return lambda *args: wrapping_get(values, indexer(*args))
  
  
def parse_config_line(line):
  """
  >>> parse_config_line('XXX = per_page Town County List.csv')
  ('Town County List.csv', 'XXX', 'per_page')
  """
  from re import search
  match = search(r'(?P<placeholder>\w+) = (?P<indexer>\w+) (?P<file>.+)', line)
  
  parsed = match.groupdict('file')
  return (parsed['file'], parsed['placeholder'], parsed['indexer'])

def table(file, column = None):
  """
  >>> table('test/Town County List.csv', 'YYY')
  ['Bedfordshire']
  """
  from csv import DictReader
  return [
    row[column] if column else list(row.values())[0]
    for row in DictReader(open(file, "r"))
  ]
  
def evaluate_generators(placeholders, page, template):
  """
  >>> evaluate_generators(
  ...  {'XXX': lambda page, invoc: str(page), 
  ...  'VVV': lambda page, invoc: str(page + invoc)},
  ...  20, 'Constant XXX and XXX, changing VVV and VVV')
  'Constant 20 and 20, changing 20 and 21'
  """
  from functools import reduce, partial
  from re import sub
  
  def sub_placeholder(rendered, placeholder_and_function):
    invocation = -1
    
    def sub_occurence(placeholder, indexer, match):
      nonlocal invocation
      invocation += 1
      return indexer(page, invocation)
    
    placeholder, indexer = placeholder_and_function
    return sub(placeholder, partial(sub_occurence, placeholder, indexer), rendered)

  return reduce(sub_placeholder, placeholders.items(), template)