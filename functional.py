from test import *
from shim import readlines, read, write, rpartial
from pipe import pipe

number_of_emails = len(list(table("Town County List.csv")))

from functools import partial

placeholders = pipe(
  readlines("config.cfg"),
  partial(map, lambda line: pipe(
    line,
    parse_config_line,
    generator
  )),
  merge
)

for n in range(number_of_emails):
  pipe(
    read("project.html"),
    rpartial(evaluate_generators, placeholders),
    partial(write, f"output/{n}.html")
  )

# Documentation

# generator(table("Cars.csv"), ["VVV"], "take")
# => { "VVV": lambda: table("Cars.csv").take()["VVV"] }

# table("Cars.csv")
# => [ {"VVV": "Ford", "VVV": "Bentley"} ]

# evaluate_generators('Once XXX, twice RRR and RRR', {'XXX': lambda: 'first', 'RRR': lambda: 'second'})
# => 'Once first, twice second and second'
