from functools import partial
from pipe import pipe
from shim import read, readlines, write

from index import access, table, evaluate_generators, parse_config_line


indexers = {
  "per_page": lambda page, invocation: page,
  "one_off":  lambda page, invocation: page + invocation
}

placeholders = pipe(
  readlines("static.cfg"),
  partial( map, parse_config_line ),
  lambda tuples: {
    placeholder: access(table(filename, placeholder), indexers[indexer])
    for filename, placeholder, indexer in tuples
  }
)

picture_sources = pipe(
  config.source_directory(),
  subdirectories,
  partial( map, lambda directory: [ directory, 
    pipe(
      directory,
      image_files,
      partial( map, image_tag_from_file )
    )
  ] ),
  lambda tuples: {
    f"[picture from #{directory}]": access(image_tags, indexers["one_off"])
    for directory, image_tags in tuples
  }
)

number_of_pages = len(table("Town County List.csv"))

def render(template, output):
  for page in range(number_of_pages):
    pipe(
      read(template),
      partial( evaluate_generators, placeholders, page ),
      partial( write, output % page )
    )
  
render("project.html", "output/%d.html")